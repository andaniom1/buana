package com.andaniom.userservice.constant;

public interface Constant extends com.andaniom.sharedlibrary.constant.Constant {

    class Message {
        public static final String USER_EXIST = "user already exist";
        public static final String USER_NOT_EXIST = "user not exist";
    }


}
