package com.andaniom.userservice.controller;

import com.andaniom.sharedlibrary.dto.UserDTO;
import com.andaniom.userservice.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping("/findByUsername")
    private ResponseEntity<?> findByUsername(@RequestParam String username){
        return ResponseEntity.ok(userService.findByUsername(username));
    }


    @PostMapping("/register")
    private ResponseEntity<?> register(@RequestBody UserDTO userDTO){
        return ResponseEntity.ok(userService.register(userDTO));
    }
}
