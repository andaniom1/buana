package com.andaniom.userservice.repository;

import com.andaniom.userservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, String> {

    @Query(value = "select * from tbl_user where username =?1 and is_deleted = false", nativeQuery = true)
    User findByUsername(String username);
}