package com.andaniom.userservice.service;


import com.andaniom.sharedlibrary.dto.UserDTO;
import com.andaniom.sharedlibrary.response.Response;

public interface UserService {


    Response<Object> register(UserDTO userDTO);

    Response<Object> findByUsername(String username);
}
