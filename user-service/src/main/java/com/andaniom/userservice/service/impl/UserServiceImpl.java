package com.andaniom.userservice.service.impl;

import com.andaniom.sharedlibrary.dto.UserDTO;
import com.andaniom.sharedlibrary.exception.DataExistException;
import com.andaniom.sharedlibrary.exception.NotFoundException;
import com.andaniom.sharedlibrary.response.Response;
import com.andaniom.userservice.constant.Constant;
import com.andaniom.userservice.entity.User;
import com.andaniom.userservice.repository.UserRepository;
import com.andaniom.userservice.service.UserService;
import jakarta.annotation.Resource;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserRepository userRepository;

    Date nowDate = new Date();

    @Override
    @Transactional
    public Response<Object> register(UserDTO userDTO) {
        User user = userRepository.findByUsername(userDTO.getUsername());
        if (Objects.nonNull(user)) {
            throw new DataExistException(Constant.Message.USER_EXIST);
        }

        User savedUser = userRepository.save(mappingUser(userDTO));
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(savedUser)
                .build();

    }

    @Override
    public Response<Object> findByUsername(String username){
        User user = userRepository.findByUsername(username);
        if (Objects.isNull(user)) {
            throw new NotFoundException(Constant.Message.USER_NOT_EXIST);
        }
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(user)
                .build();
    }

    private User mappingUser(UserDTO userDTO) {
        return User.builder()
                .name(userDTO.getName())
                .phoneNumber(userDTO.getPhoneNumber())
                .address(userDTO.getAddress())
                .email(userDTO.getEmail())
                .username(userDTO.getUsername())
                .password(userDTO.getPassword())
                .role(userDTO.getRole())
                .createdDate(this.nowDate)
                .isDeleted(false)
                .build();
    }
}

