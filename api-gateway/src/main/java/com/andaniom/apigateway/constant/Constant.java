package com.andaniom.apigateway.constant;

import java.util.List;

public interface Constant {

    List<String> AUTH_WHITELIST = List.of(
            "/swagger-ui/**",
            "/api-docs/**",
            "/swagger-ui.html",
            "/auth/register",
            "/auth/login",
            "/eureka"
    );

    class Message {
        public static final String FORBIDDEN_MESSAGE = "You don't have access";
        public static final String INVALID_TOKEN_MESSAGE = "Invalid access token";
    }
}
