package com.andaniom.sharedlibrary.service;

import lombok.Data;

@Data
public class JwtPayload {
    public String sub;
    public Long iat;
    public Long exp;
    public String role;
    public String id;
    public String username;
}
