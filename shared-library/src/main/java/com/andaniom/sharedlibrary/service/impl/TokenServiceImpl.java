package com.andaniom.sharedlibrary.service.impl;

import com.andaniom.sharedlibrary.aspect.AccessTokenContext;
import com.andaniom.sharedlibrary.service.JwtPayload;
import com.andaniom.sharedlibrary.service.TokenService;
import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Base64;
import java.util.Objects;

@Service
public class TokenServiceImpl implements TokenService {

    public String getAccessToken() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder
                .getRequestAttributes())).getRequest();
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }

    @Override
    public AccessTokenContext getLoginTokenContext(String token) {
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String[] chunks = token.split("\\.");
        String payload = new String(decoder.decode(chunks[1]));
        JwtPayload jwtPayload = new Gson().fromJson(payload, JwtPayload.class);
        AccessTokenContext tokenContext = new AccessTokenContext();
        tokenContext.setUserId(jwtPayload.getId());
        tokenContext.setUsername(jwtPayload.getUsername());
        tokenContext.setExpireIn(jwtPayload.getExp());
        tokenContext.setRole(jwtPayload.getRole());
        return tokenContext;
    }
}
