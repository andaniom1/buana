package com.andaniom.sharedlibrary.service;


import com.andaniom.sharedlibrary.aspect.AccessTokenContext;

public interface TokenService {

    String getAccessToken();

    AccessTokenContext getLoginTokenContext(String token);
}
