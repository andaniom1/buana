package com.andaniom.sharedlibrary.request;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class QueryProductRequest {
    private int pageNumber;
    private int pageSize;
    private String productName;

}
