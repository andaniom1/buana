package com.andaniom.sharedlibrary.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO for user entity
 */
@Data
@Builder
public class UserDTO implements Serializable {
    private String id;
    private String name;
    private String phoneNumber;
    private String address;
    private String email;
    private String username;
    private String password;
    private String role;
    private Date createdDate;
    private Date modifiedDate;
    private Boolean isDeleted;
}