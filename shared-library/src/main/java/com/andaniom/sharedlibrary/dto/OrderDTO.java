package com.andaniom.sharedlibrary.dto;

import lombok.Value;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * DTO for {@link com.andaniom.orderservice.entity.Order}
 */
@Value
public class OrderDTO implements Serializable {
    String id;
    String orderNo;
    String userId;
    String username;
    String productId;
    String productName;
    Integer status;
    int quantity;
    BigDecimal unitPrice;
    BigDecimal totalAmount;
    Date paidDate;
    Date createdDate;
    Date modifiedDate;
    Boolean isDeleted;
}