package com.andaniom.sharedlibrary.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class ProductDTO implements Serializable {
    private String id;
    private String name;
    private BigDecimal price;
    private String description;
    private String userId;
    private Integer status;
    private int quantity;
    private Date createdDate;
    private Date modifiedDate;
    private Boolean isDeleted;
}
