package com.andaniom.sharedlibrary.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class InventoryDTO implements Serializable {
    private String id;
    private String productId;
    private int stock;
    private int reservedStock;
    private String status;
    private Date createdDate;
    private Date modifiedDate;
}
