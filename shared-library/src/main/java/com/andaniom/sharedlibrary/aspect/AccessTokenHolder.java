package com.andaniom.sharedlibrary.aspect;

public class AccessTokenHolder {

    private static ThreadLocal<AccessTokenContext> tokenThreadLocal = new ThreadLocal<>();

    public static AccessTokenContext getAccessTokenContext() {
        return tokenThreadLocal.get();
    }

    public static void setAccessTokenContext(AccessTokenContext accessTokenContext) {
        tokenThreadLocal.set(accessTokenContext);
    }

    public static void clear() {
        tokenThreadLocal.remove();
    }
}
