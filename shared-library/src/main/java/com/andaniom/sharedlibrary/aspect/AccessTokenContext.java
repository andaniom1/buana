package com.andaniom.sharedlibrary.aspect;

import lombok.Data;

import java.io.Serializable;

@Data
public class AccessTokenContext implements Serializable {

    private String userId;

    private String username;

    private Long expireIn;

    private String role;
}
