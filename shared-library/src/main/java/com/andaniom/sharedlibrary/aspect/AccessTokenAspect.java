package com.andaniom.sharedlibrary.aspect;

import com.andaniom.sharedlibrary.constant.Constant;
import com.andaniom.sharedlibrary.exception.SystemException;
import com.andaniom.sharedlibrary.service.TokenService;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.Resource;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(0)
@Aspect
@Component
public class AccessTokenAspect {

    private static final Logger logger = LoggerFactory.getLogger(AccessTokenAspect.class);

    @Resource
    private TokenService tokenService;

    @Around("@annotation(com.andaniom.sharedlibrary.annotation.AccessToken)")
    public Object aroundAccessTokenAnnotation(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info("doAccessCheck");
        try {
            String accessToken = tokenService.getAccessToken();
            logger.info("accessToken : {}", accessToken);
            if (StringUtils.isEmpty(accessToken)) {
                logger.error("get token empty");
                throw new SystemException(Constant.Message.INVALID_TOKEN_MESSAGE);
            }

            AccessTokenContext accessTokenContext = tokenService.getLoginTokenContext(accessToken);
            AccessTokenHolder.setAccessTokenContext(accessTokenContext);

            return joinPoint.proceed();
        } catch (Exception e) {
            throw e;
        } finally {
            AccessTokenHolder.clear();
        }
    }
}
