package com.andaniom.sharedlibrary.aspect;

import com.andaniom.sharedlibrary.constant.Constant;
import com.andaniom.sharedlibrary.exception.UnauthorizedException;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Order(1)
@Aspect
@Component
public class PermissionCheckCustomAspect {
    private static final Logger logger =  LoggerFactory.getLogger(PermissionCheckCustomAspect.class);

    private static final Set<String> urlSet = new HashSet<>() {
        {
            add("/product/create");
            add("/inventory/updateStock");
            add("/inventory/adjustStock");
        }
    };

    @Around("@annotation(com.andaniom.sharedlibrary.annotation.PermissionCheckCustom)")
    public Object doPermissionCheck(ProceedingJoinPoint pjt) throws Throwable{
        AccessTokenContext tokenContext = AccessTokenHolder.getAccessTokenContext();
        String userId = tokenContext.getUserId();
        String role = tokenContext.getRole();
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder
                .getRequestAttributes())).getRequest();
        String requestURI = request.getRequestURI();
        Object[] args = pjt.getArgs();
        logger.info("userId = {}, role : {}, requestURL = {}", userId, role, requestURI);
        logger.info(urlSet.toString());
        logger.info(String.valueOf(urlSet.contains(requestURI)));

        if(!role.equals("ROLE_SELLER") || !urlSet.contains(requestURI)){
            throw new UnauthorizedException(Constant.Message.FORBIDDEN_MESSAGE);
        }

        return pjt.proceed();
    }
}
