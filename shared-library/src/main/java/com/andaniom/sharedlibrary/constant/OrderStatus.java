package com.andaniom.sharedlibrary.constant;

public class OrderStatus {
    public static final int WAIT_PAYMENT = 1;
    public static final int SUCCESS = 2;
    public static final int CANCEL = 3;
}
