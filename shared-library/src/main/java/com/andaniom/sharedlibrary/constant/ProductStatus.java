package com.andaniom.sharedlibrary.constant;

public class ProductStatus {
    public static final int ACTIVE = 1;
    public static final int INACTIVE = 2;
    public static final int OUT_OF_STOCK = 3;
}
