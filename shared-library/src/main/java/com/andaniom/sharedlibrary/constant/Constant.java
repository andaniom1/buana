package com.andaniom.sharedlibrary.constant;

public interface Constant {

    class Response {
        public static final int SUCCESS_CODE = 200;
        public static final String SUCCESS_MESSAGE = "Success";
    }

    class Message {
        public static final String FORBIDDEN_MESSAGE = "You don't have access";
        public static final String INVALID_TOKEN_MESSAGE = "Invalid access token";
    }

    class Regex {
        public static final String NUMERIC = "\\d+";
        public static final String ALPHANUMERIC = "^[a-zA-Z0-9]+$";
        public static final String ALPHABET = "^[a-zA-Z]+$";
        public static final String ALPHANUMERIC_WITH_DOT_AND_SPACE = "^[a-zA-Z0-9.' ]+$";
    }

}
