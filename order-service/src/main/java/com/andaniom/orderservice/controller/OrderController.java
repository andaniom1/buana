package com.andaniom.orderservice.controller;

import com.andaniom.orderservice.request.CancelOrderRequest;
import com.andaniom.orderservice.request.CreateOrderRequest;
import com.andaniom.orderservice.request.OrderRequest;
import com.andaniom.orderservice.request.PaymentOrderRequest;
import com.andaniom.orderservice.service.OrderService;
import com.andaniom.sharedlibrary.annotation.AccessToken;
import com.andaniom.sharedlibrary.aspect.AccessTokenContext;
import com.andaniom.sharedlibrary.aspect.AccessTokenHolder;
import jakarta.annotation.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderService orderService;

    @AccessToken
    @PostMapping("/create")
    public ResponseEntity<Object> create(@RequestBody CreateOrderRequest request) {
        AccessTokenContext context = AccessTokenHolder.getAccessTokenContext();
        request.setUserId(context.getUserId());
        request.setUsername(context.getUsername());
        return ResponseEntity.ok(orderService.create(request));
    }

    @AccessToken
    @PostMapping("/list")
    public ResponseEntity<Object> orderList(@RequestBody OrderRequest request) {
        AccessTokenContext context = AccessTokenHolder.getAccessTokenContext();
        request.setUserId(context.getUserId());
        return ResponseEntity.ok(orderService.orderList(request));
    }

    @AccessToken
    @PostMapping("/payment")
    public ResponseEntity<Object> payment(@RequestBody PaymentOrderRequest request) {
        AccessTokenContext context = AccessTokenHolder.getAccessTokenContext();
        request.setUserId(context.getUserId());
        return ResponseEntity.ok(orderService.paymentOrder(request));
    }

    @AccessToken
    @PostMapping("/cancel")
    public ResponseEntity<Object> cancel(@RequestBody CancelOrderRequest request) {
        AccessTokenContext context = AccessTokenHolder.getAccessTokenContext();
        request.setUserId(context.getUserId());
        return ResponseEntity.ok(orderService.cancelOrder(request));
    }
}
