package com.andaniom.orderservice.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class CancelOrderRequest {
    @NotBlank(message = "orderId is mandatory, please fill it!")
    private String orderId;
    private String userId;
}
