package com.andaniom.orderservice.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PaymentOrderRequest {

    @NotBlank(message = "orderId is mandatory, please fill it!")
    private String orderId;

    @NotNull(message = "amount is mandatory, please fill it!")
    private BigDecimal amount;

    private String userId;
}
