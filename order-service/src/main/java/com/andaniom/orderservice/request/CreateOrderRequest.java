package com.andaniom.orderservice.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CreateOrderRequest {
    private String userId;
    private String username;
    private String productId;
    private Integer quantity;
}
