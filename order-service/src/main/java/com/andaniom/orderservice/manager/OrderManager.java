package com.andaniom.orderservice.manager;

import com.andaniom.orderservice.entity.Order;
import com.andaniom.orderservice.request.CreateOrderRequest;
import com.andaniom.sharedlibrary.constant.OrderStatus;
import com.andaniom.sharedlibrary.dto.InventoryDTO;
import com.andaniom.sharedlibrary.dto.ProductDTO;
import com.andaniom.sharedlibrary.response.Response;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Component
@Slf4j
public class OrderManager {

    @Resource
    private RestTemplate restTemplate;

    @Value("${service.product.url}")
    private String productUrl;

    @Value("${service.inventory.url}")
    private String inventoryUrl;

    /*
     * Get product-service Product by product id
     */
    public ProductDTO getProduct(String productId, String userId) {
        try {
            ResponseEntity<Response<ProductDTO>> responseEntity = restTemplate.exchange(
                    productUrl + "/getByIdAndUserId?id={productId}&userId={userId}",
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    },
                    productId, userId
            );

            if (responseEntity.getStatusCode().is2xxSuccessful() && Objects.nonNull(responseEntity.getBody())) {
                return responseEntity.getBody().getData();
            } else {
                return null;
            }
        } catch (RestClientException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /*
     * Get inventory-service Inventory by product id
     */
    public InventoryDTO getInventory(String productId) {
        try {
            ResponseEntity<Response<InventoryDTO>> responseEntity = restTemplate.exchange(
                    inventoryUrl + "/stock?productId={productId}",
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    },
                    productId
            );

            if (responseEntity.getStatusCode().is2xxSuccessful() && Objects.nonNull(responseEntity.getBody())) {
                return responseEntity.getBody().getData();
            } else {
                return null;
            }
        } catch (RestClientException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public Order mappingOrder(CreateOrderRequest request, ProductDTO productDTO) {
        BigDecimal totalAmount = productDTO.getPrice().multiply(BigDecimal.valueOf(request.getQuantity()));
        return Order.builder()
                .userId(request.getUserId())
                .username(request.getUsername())
                .totalAmount(totalAmount)
                .productId(request.getProductId())
                .productName(productDTO.getName())
                .status(OrderStatus.WAIT_PAYMENT)
                .quantity(request.getQuantity())
                .unitPrice(productDTO.getPrice())
                .createdDate(new Date())
                .modifiedDate(new Date())
                .isDeleted(false)
                .build();
    }
}
