package com.andaniom.orderservice.constant;

public interface Constant extends com.andaniom.sharedlibrary.constant.Constant {

    class Message {
        public static final String ORDER_NOT_FOUND = "order not found";
        public static final String ORDER_PAYMENT_AMOUNT_ERROR = "the amount paid is not correct";
        public static final String ORDER_STATUS_ERROR = "status error";
        public static final String PRODUCT_NOT_FOUND = "product not found";
        public static final String PRODUCT_NOT_ACTIVE = "product not active";
        public static final String INVENTORY_PRODUCT_NOT_FOUND = "inventory product not found";
        public static final String INVENTORY_PRODUCT_INSUFFICIENT_STOCK = "inventory product Insufficient Stock";
        public static final String FORBIDDEN_REQUEST_MESSAGE = "Different {value} with exist data is forbidden";
    }


}
