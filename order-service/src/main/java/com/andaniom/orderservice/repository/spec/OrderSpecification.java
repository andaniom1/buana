package com.andaniom.orderservice.repository.spec;

import com.andaniom.orderservice.entity.Order;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class OrderSpecification {

    public static Specification<Order> findByUserIdAndIsDeleted(String userId, boolean isDeleted) {
        return (Root<Order> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {
            Predicate userIdPredicate = criteriaBuilder.equal(root.get("userId"), userId);
            Predicate isDeletedPredicate = criteriaBuilder.equal(root.get("isDeleted"), isDeleted);
            return criteriaBuilder.and(userIdPredicate, isDeletedPredicate);
        };
    }
}
