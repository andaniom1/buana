package com.andaniom.orderservice.repository;

import com.andaniom.orderservice.entity.Order;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface OrderRepository extends JpaRepository<Order, String>, JpaSpecificationExecutor<Order> {

    @Query(value = "select o.* from tbl_order o join tbl_order_detail d on (o.id = d.order_id) where o.is_deleted = false order by o.created_date desc", nativeQuery = true)
    Page<Order> findPerPage(Pageable pageable);

    @NonNull Page<Order> findAll(@NonNull Specification<Order> spec, @NonNull Pageable pageable);



}