package com.andaniom.orderservice.service;

import com.andaniom.orderservice.request.CancelOrderRequest;
import com.andaniom.orderservice.request.CreateOrderRequest;
import com.andaniom.orderservice.request.OrderRequest;
import com.andaniom.orderservice.request.PaymentOrderRequest;
import com.andaniom.sharedlibrary.response.Response;

public interface OrderService {

    Response<Object> create(CreateOrderRequest request);

    Response<Object> orderList(OrderRequest request);

    Response<Object> paymentOrder(PaymentOrderRequest request);

    Response<Object> cancelOrder(CancelOrderRequest request);
}
