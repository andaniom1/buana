package com.andaniom.orderservice.service.impl;

import com.andaniom.orderservice.constant.Constant;
import com.andaniom.orderservice.entity.Order;
import com.andaniom.orderservice.manager.OrderManager;
import com.andaniom.orderservice.message.producer.OrderProducer;
import com.andaniom.orderservice.repository.OrderRepository;
import com.andaniom.orderservice.repository.spec.OrderSpecification;
import com.andaniom.orderservice.request.CancelOrderRequest;
import com.andaniom.orderservice.request.CreateOrderRequest;
import com.andaniom.orderservice.request.OrderRequest;
import com.andaniom.orderservice.request.PaymentOrderRequest;
import com.andaniom.orderservice.service.OrderService;
import com.andaniom.sharedlibrary.constant.OrderStatus;
import com.andaniom.sharedlibrary.constant.ProductStatus;
import com.andaniom.sharedlibrary.dto.InventoryDTO;
import com.andaniom.sharedlibrary.dto.ProductDTO;
import com.andaniom.sharedlibrary.exception.BadRequestCustomException;
import com.andaniom.sharedlibrary.exception.NotFoundException;
import com.andaniom.sharedlibrary.response.Response;
import com.google.gson.Gson;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderRepository orderRepository;

    @Resource
    private OrderManager orderManager;

    @Resource
    private OrderProducer orderProducer;

    @Value("${kafka.topic.order.create-order}")
    private String createOrderTopic;

    @Value("${kafka.topic.order.success-order}")
    private String successOrderTopic;

    @Value("${kafka.topic.order.cancel-order}")
    private String cancelOrderTopic;


    @Override
    public Response<Object> create(CreateOrderRequest request) {
        ProductDTO productDTO = orderManager.getProduct(request.getProductId(), request.getUserId());
        if (Objects.isNull(productDTO)) {
            throw new NotFoundException(Constant.Message.PRODUCT_NOT_FOUND);
        }

        if (!productDTO.getStatus().equals(ProductStatus.ACTIVE)) {
            throw new BadRequestCustomException(Constant.Message.PRODUCT_NOT_ACTIVE);
        }

        InventoryDTO inventoryDTO = orderManager.getInventory(request.getProductId());
        if (Objects.isNull(inventoryDTO)) {
            throw new NotFoundException(Constant.Message.INVENTORY_PRODUCT_NOT_FOUND);
        }

        if (inventoryDTO.getStock() < request.getQuantity()) {
            throw new NotFoundException(Constant.Message.INVENTORY_PRODUCT_INSUFFICIENT_STOCK);
        }

        Order order = orderRepository.save(orderManager.mappingOrder(request, productDTO));

        orderProducer.sendMessage(createOrderTopic, new Gson().toJson(order));
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(order)
                .build();
    }

    @Override
    public Response<Object> orderList(OrderRequest request) {
        Pageable paging = PageRequest.of(request.getPageNumber(), request.getPageSize());
        Specification<Order> spec = OrderSpecification.findByUserIdAndIsDeleted(request.getUserId(), false);
        Page<Order> orderPage = orderRepository.findAll(spec, paging);
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(Objects.nonNull(orderPage) ? orderPage.getContent() : new ArrayList<>())
                .totalPage(Objects.nonNull(orderPage) ? orderPage.getTotalPages() : null)
                .totalData(Objects.nonNull(orderPage) ? orderPage.getTotalElements() : null)
                .pageNumber(request.getPageNumber())
                .pageSize(request.getPageSize())
                .build();
    }

    @Override
    public Response<Object> paymentOrder(PaymentOrderRequest request) {
        Order order = getProduct(request.getOrderId());

        if (!request.getAmount().equals(order.getTotalAmount())) {
            throw new BadRequestCustomException(Constant.Message.ORDER_PAYMENT_AMOUNT_ERROR);
        }

        order = orderRepository.save(Order.builder()
                .id(order.getId())
                .status(OrderStatus.SUCCESS)
                .paidDate(new Date())
                .modifiedDate(new Date())
                .build());

        // send mq confirm reserve stock
        orderProducer.sendMessage(successOrderTopic, new Gson().toJson(order));

        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(order)
                .build();
    }

    @Override
    public Response<Object> cancelOrder(CancelOrderRequest request) {
        Order order = getProduct(request.getOrderId());

        order = orderRepository.save(Order.builder()
                .id(order.getId())
                .status(OrderStatus.CANCEL)
                .modifiedDate(new Date())
                .build());

        // send mq confirm reserve stock
        orderProducer.sendMessage(cancelOrderTopic, new Gson().toJson(order));

        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(order)
                .build();
    }

    private Order getProduct(String orderId) {
        Optional<Order> optionalOrder = orderRepository.findById(orderId);
        if (optionalOrder.isEmpty()) {
            throw new NotFoundException(Constant.Message.ORDER_NOT_FOUND);
        }

        Order order = optionalOrder.get();
        if (!order.getStatus().equals(OrderStatus.WAIT_PAYMENT)) {
            throw new BadRequestCustomException(Constant.Message.ORDER_STATUS_ERROR);
        }

        return order;
    }
}
