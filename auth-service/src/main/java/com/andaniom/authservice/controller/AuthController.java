package com.andaniom.authservice.controller;

import com.andaniom.authservice.request.LoginRequest;
import com.andaniom.authservice.service.AuthService;
import com.andaniom.authservice.request.RegisterRequest;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    @Resource
    private AuthService userService;

    @PostMapping(value = "/register")
    public ResponseEntity<Object> register(@Valid @RequestBody RegisterRequest registerRequest) {
        return ResponseEntity.ok(userService.register(registerRequest));
    }

    @PostMapping(value = "/login")
    public ResponseEntity<Object> login(@Valid @RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(userService.login(loginRequest));
    }

    @GetMapping(value = "/validateAccessToken")
    public ResponseEntity<Object> validateAccessToken(@RequestParam(value = "accessToken", defaultValue = "")String accessToken) {
        return ResponseEntity.ok(userService.validateAccessToken(accessToken));
    }

    @GetMapping(value = "/test")
    public ResponseEntity<Object> doTest() {
        return ResponseEntity.ok("Success Test");
    }
}
