package com.andaniom.authservice.security.service;

import com.andaniom.authservice.manager.AuthManager;
import com.andaniom.authservice.constant.Constant;
import com.andaniom.sharedlibrary.dto.UserDTO;
import jakarta.annotation.Resource;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Resource
    private AuthManager authManager;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDTO user = authManager.findByUsername(username);
        if(Objects.isNull(user)) {
            throw new UsernameNotFoundException(Constant.Message.NOT_FOUND_DATA_MESSAGE);
        }
        log.info("loadUserByUsername : {}", user.getUsername());
        return UserDetailsImpl.build(user);
    }

    private static Collection<? extends GrantedAuthority> getAuthorities(String roles) {
        Set<GrantedAuthority> authorities = new HashSet<>();

        for (String role : roles.split("//|")) {
            authorities.add(new SimpleGrantedAuthority(role));
        }

        return authorities;
    }
}