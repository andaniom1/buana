package com.andaniom.authservice.manager;

import com.andaniom.sharedlibrary.dto.UserDTO;
import com.andaniom.sharedlibrary.response.Response;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Component
public class AuthManager {

    @Resource
    private RestTemplate restTemplate;

    @Value("${service.user.url}")
    private String userUrl;


    public UserDTO findByUsername(String username) {
        try {
            ResponseEntity<Response<UserDTO>> responseEntity = restTemplate.exchange(
                    userUrl + "/findByUsername?username={username}",
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<>() {
                    },
                    username
            );

            if (responseEntity.getStatusCode().is2xxSuccessful() && Objects.nonNull(responseEntity.getBody())) {
                return responseEntity.getBody().getData();
            } else {
                return null;
            }
        } catch (RestClientException e) {
            return null;
        }
    }


    public UserDTO register(UserDTO userDTO) {
        HttpEntity<UserDTO> httpEntity = new HttpEntity<>(userDTO);
        ResponseEntity<Response<UserDTO>> responseEntity = restTemplate.exchange(
                userUrl + "/register",
                HttpMethod.POST,
                httpEntity,
                new ParameterizedTypeReference<>() {
                }
        );

        if (responseEntity.getStatusCode().is2xxSuccessful() && Objects.nonNull(responseEntity.getBody())) {
            return responseEntity.getBody().getData();
        } else {
            return null;
        }
    }

}
