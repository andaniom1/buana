package com.andaniom.authservice.service.impl;

import com.andaniom.authservice.constant.Constant;
import com.andaniom.authservice.manager.AuthManager;
import com.andaniom.authservice.request.LoginRequest;
import com.andaniom.authservice.request.RegisterRequest;
import com.andaniom.authservice.response.LoginResponse;
import com.andaniom.authservice.security.jwt.JwtUtils;
import com.andaniom.authservice.security.service.UserDetailsImpl;
import com.andaniom.authservice.service.AuthService;
import com.andaniom.sharedlibrary.dto.UserDTO;
import com.andaniom.sharedlibrary.exception.BadRequestCustomException;
import com.andaniom.sharedlibrary.exception.DataExistException;
import com.andaniom.sharedlibrary.exception.NotFoundException;
import com.andaniom.sharedlibrary.response.Response;
import jakarta.annotation.Resource;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class AuthServiceImpl implements AuthService {
    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private AuthenticationManager authenticationManager;

    @Resource
    private AuthManager authManager;
    @Resource
    private JwtUtils jwtUtils;

    @Value("${jwt.expirationMs}")
    private int jwtExpirationMs;

    @Value("${user.role}")
    private String userRole;

    Date nowDate = new Date();

    @Override
    @Transactional
    public Response<Object> register(RegisterRequest registerRequest) {
        UserDTO user = authManager.findByUsername(registerRequest.getUsername());
        if (Objects.nonNull(user)) {
            throw new DataExistException(Constant.Message.EXIST_DATA_MESSAGE);
        }

        if (StringUtils.isEmpty(registerRequest.getRole())) {
            registerRequest.setRole("ROLE_BUYER");
        }

        if (!isValid(userRole, registerRequest.getRole())) {
            throw new BadRequestCustomException(Constant.Message.FORBIDDEN_REQUEST_MESSAGE.replace("{value}", "role"));
        }

        UserDTO savedUser = authManager.register(mappingUser(registerRequest));

        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(savedUser)
                .build();

    }


    @Override
    public Response<Object> login(LoginRequest loginRequest) {
        try {
            UserDTO user = authManager.findByUsername(loginRequest.getUsername());
            if (Objects.isNull(user)) {
                throw new NotFoundException(Constant.Message.INVALID_LOGIN_MESSAGE);
            }

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);
            log.info("jwt : {}", jwt);
            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

            LoginResponse response = mappingLoginResponse(user, jwt, userDetails.getRole());
            return Response.builder()
                    .responseCode(Constant.Response.SUCCESS_CODE)
                    .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                    .data(response)
                    .build();
        } catch (BadCredentialsException e) {
            log.error("Authentication failed: Bad credentials : {}", e.getMessage());
            throw new RuntimeException(e);
        } catch (NotFoundException | AuthenticationException e) {
            log.error("Authentication failed : {}", e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public Response<Object> validateAccessToken(String accessToken) {
        boolean isValid = jwtUtils.validateJwtToken(accessToken);
        if (!isValid) {
            throw new BadRequestCustomException(Constant.Message.INVALID_TOKEN_MESSAGE);
        }
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_VALID_TOKEN_MESSAGE)
                .build();
    }

    private LoginResponse mappingLoginResponse(UserDTO user, String jwt, String role) {
        return LoginResponse.builder()
                .userId(user.getId())
                .username(user.getUsername())
                .accessToken(jwt)
                .tokenType("Bearer")
                .expiresIn(jwtExpirationMs)
                .role(role)
                .build();
    }

    private UserDTO mappingUser(RegisterRequest registerRequest) {
        return UserDTO.builder()
                .name(registerRequest.getName())
                .phoneNumber(registerRequest.getPhoneNumber())
                .address(registerRequest.getAddress())
                .email(registerRequest.getEmail())
                .username(registerRequest.getUsername())
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .role(registerRequest.getRole())
                .createdDate(this.nowDate)
                .isDeleted(false)
                .build();
    }

    private boolean isValid(String value, String request) {
        String[] valueList = value.split("\\|");
        List<String> reqList = new ArrayList<>();
        for (int i = 0; i < valueList.length; i++) {
            if (valueList[i].equals(request)) {
                reqList.add(request);
                break;
            }
        }
        if (reqList.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }
}

