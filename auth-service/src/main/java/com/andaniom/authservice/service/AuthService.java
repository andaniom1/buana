package com.andaniom.authservice.service;

import com.andaniom.authservice.request.LoginRequest;
import com.andaniom.authservice.request.RegisterRequest;

public interface AuthService {


    Object register(RegisterRequest registerRequest);

    Object login(LoginRequest loginRequest);

    Object validateAccessToken(String accessToken);
}
