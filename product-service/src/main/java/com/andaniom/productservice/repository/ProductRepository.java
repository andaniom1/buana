package com.andaniom.productservice.repository;

import com.andaniom.productservice.entity.Product;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

public interface ProductRepository extends JpaRepository<Product, String> {

    @Query(value = "select p.* from tbl_product p where lower(p.name) like ?1 " +
            "and p.is_deleted = false order by p.created_date asc", nativeQuery = true)
    Page<Product> findPerPage(String name, Pageable pageable);

    @Query(value = "select * from tbl_product where id =?1 and is_deleted = false", nativeQuery = true)
    Product findByProductId(String id);

    @Query(value = "select * from tbl_product where id =?1 and user_id =?2 and is_deleted = false", nativeQuery = true)
    Product findByIdAndUserId(String id, String userId);

    @Transactional
    @Modifying
    @Query(value = "update tbl_product set is_deleted = true, modified_by = ?2, modified_date = ?3 where id = ?1", nativeQuery = true)
    void deleteById(String id, String modifiedBy, Date modifiedDate);
}