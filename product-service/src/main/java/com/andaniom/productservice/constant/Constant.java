package com.andaniom.productservice.constant;

import java.text.SimpleDateFormat;
import java.util.Locale;

public interface Constant extends com.andaniom.sharedlibrary.constant.Constant {

    class Message {
        public static final String EXIST_DATA_MESSAGE = "data already exist";
        public static final String PRODUCT_NOT_FOUND = "product not found";
        public static final String FORBIDDEN_REQUEST_MESSAGE = "Different {value} with exist data is forbidden";
    }


}
