package com.andaniom.productservice.service.impl;

import com.andaniom.productservice.constant.Constant;
import com.andaniom.productservice.entity.Product;
import com.andaniom.productservice.manager.ProductManager;
import com.andaniom.productservice.message.producer.ProductProducer;
import com.andaniom.productservice.repository.ProductRepository;
import com.andaniom.productservice.request.ProductRequest;
import com.andaniom.productservice.request.QueryProductRequest;
import com.andaniom.productservice.service.ProductService;
import com.andaniom.sharedlibrary.exception.DataExistException;
import com.andaniom.sharedlibrary.exception.NotFoundException;
import com.andaniom.sharedlibrary.response.Response;
import com.google.gson.Gson;
import jakarta.annotation.Resource;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

@Service
public class ProductServiceImpl implements ProductService {

    @Resource
    private ProductRepository productRepository;

    @Resource
    private ProductManager productManager;

    @Resource
    private ProductProducer productProducer;

    @Value("${kafka.topic.product.create-product}")
    private String createOrderTopic;

    @Override
    @Transactional
    public Response<Object> create(ProductRequest request) {
        Product product = productRepository.save(productManager.mappingProduct(request));;
        // send mq init inventory
        productProducer.sendMessage(createOrderTopic, new Gson().toJson(product));
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(product)
                .build();
    }

    @Override
    public Response<Object> getProduct(QueryProductRequest request) {
        String name = productManager.filterMapping(request.getProductName());
        Pageable paging = PageRequest.of(request.getPageNumber(), request.getPageSize());
        Page<Product> productPage = productRepository.findPerPage(name, paging);
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(Objects.nonNull(productPage) ? productPage.getContent() : new ArrayList<>())
                .totalPage(Objects.nonNull(productPage) ? productPage.getTotalPages() : null)
                .totalData(Objects.nonNull(productPage) ?  productPage.getTotalElements() : null)
                .pageNumber(request.getPageNumber())
                .pageSize(request.getPageSize())
                .build();
    }

    @Override
    public Response<Object> getById(String id) {
        Product product = productRepository.findByProductId(id);
        if(Objects.isNull(product)) {
            throw new NotFoundException(Constant.Message.PRODUCT_NOT_FOUND);
        }

        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(product)
                .build();
    }

    @Override
    @Transactional
    public Response<Object> update(String id, ProductRequest productRequest) {
        Product product = productRepository.findByIdAndUserId(id, productRequest.getUserId());
        if (Objects.isNull(product)) {
            throw new NotFoundException(Constant.Message.PRODUCT_NOT_FOUND);
        }

        Product updatedProduct = Product.builder()
                .id(id)
                .name(productRequest.getName())
                .price(productRequest.getPrice())
                .description(productRequest.getDescription())
                .createdDate(product.getCreatedDate())
                .modifiedDate(new Date())
                .isDeleted(product.getIsDeleted())
                .build();
        productRepository.save(updatedProduct);

        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(updatedProduct)
                .build();
    }

    @Override
    @Transactional
    public Response<Object> delete(String id, String username) {
        Product product = productRepository.findByProductId(id);
        if (Objects.isNull(product)) {
            throw new NotFoundException(Constant.Message.PRODUCT_NOT_FOUND);
        }

        productRepository.deleteById(id, username, new Date());
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .build();
    }

    @Override
    public Response<Object> getByIdAndUserId(String id, String userId) {
        Product product = productRepository.findByIdAndUserId(id, userId);
        if(Objects.isNull(product)) {
            throw new NotFoundException(Constant.Message.PRODUCT_NOT_FOUND);
        }

        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(product)
                .build();
    }
}
