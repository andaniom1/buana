package com.andaniom.productservice.service;

import com.andaniom.productservice.request.ProductRequest;
import com.andaniom.productservice.request.QueryProductRequest;
import com.andaniom.sharedlibrary.response.Response;

public interface ProductService {

    Response<Object> create(ProductRequest request);

    Response<Object> getProduct(QueryProductRequest request);

    Response<Object> getById(String id);

    Response<Object> update(String id, ProductRequest productRequest);

    Response<Object> delete(String id, String username);

    Response<Object> getByIdAndUserId(String id, String userId);
}
