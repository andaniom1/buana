package com.andaniom.productservice.manager;

import com.andaniom.productservice.entity.Product;
import com.andaniom.productservice.request.ProductRequest;
import com.andaniom.sharedlibrary.constant.ProductStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.Date;
import java.util.Objects;

@Component
public class ProductManager {

    public Product mappingProduct(ProductRequest productRequest) {
        return Product.builder()
                .name(productRequest.getName())
                .price(productRequest.getPrice())
                .description(productRequest.getDescription())
                .userId(productRequest.getUserId())
                .status(ProductStatus.ACTIVE)
                .quantity(productRequest.getQuantity())
                .createdDate(new Date())
                .isDeleted(false)
                .build();
    }

    public String filterMapping(String value) {
        if(Objects.isNull(value) || ObjectUtils.isEmpty(value.trim())) {
            return "%%";
        } else {
            return "%".concat(value.toLowerCase()).concat("%");
        }
    }
}
