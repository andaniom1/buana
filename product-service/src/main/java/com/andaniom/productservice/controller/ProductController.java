package com.andaniom.productservice.controller;

import com.andaniom.sharedlibrary.annotation.AccessToken;
import com.andaniom.productservice.request.ProductRequest;
import com.andaniom.productservice.request.QueryProductRequest;
import com.andaniom.productservice.service.ProductService;
import com.andaniom.sharedlibrary.annotation.PermissionCheckCustom;
import com.andaniom.sharedlibrary.aspect.AccessTokenContext;
import com.andaniom.sharedlibrary.aspect.AccessTokenHolder;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/product")
public class ProductController {

    @Resource
    private ProductService productService;

    @AccessToken
    @PermissionCheckCustom
    @PostMapping(value = "/create")
    public ResponseEntity<Object> create(@Valid @RequestBody @NonNull ProductRequest productRequest) {
        AccessTokenContext context = AccessTokenHolder.getAccessTokenContext();
        productRequest.setUserId(context.getUserId());
        productRequest.setUsername(context.getUsername());
        return ResponseEntity.ok(productService.create(productRequest));
    }

    @AccessToken
    @PostMapping(value = "/products")
    public ResponseEntity<Object> getProduct(@Valid @RequestBody QueryProductRequest request) {
        return ResponseEntity.ok(productService.getProduct(request));
    }

    @GetMapping(value = "/getById")
    public ResponseEntity<Object> getById(@RequestParam(value = "id", defaultValue = "")String id) {
        return ResponseEntity.ok(productService.getById(id));
    }

    @GetMapping(value = "/getByIdAndUserId")
    public ResponseEntity<Object> getByIdAndUserId(@RequestParam String id, @RequestParam String userId) {
        return ResponseEntity.ok(productService.getByIdAndUserId(id, userId));
    }

    @AccessToken
    @PutMapping(value = "/update/{id}")
    public ResponseEntity<Object> update(@PathVariable String id, @Valid @RequestBody ProductRequest productRequest) {
        AccessTokenContext context = AccessTokenHolder.getAccessTokenContext();
        productRequest.setUserId(context.getUserId());
        productRequest.setUsername(context.getUsername());
        return ResponseEntity.ok(productService.update(id, productRequest));
    }
}
