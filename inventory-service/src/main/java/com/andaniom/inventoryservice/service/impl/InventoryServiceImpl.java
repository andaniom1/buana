package com.andaniom.inventoryservice.service.impl;

import com.andaniom.inventoryservice.constant.Constant;
import com.andaniom.inventoryservice.entity.Inventory;
import com.andaniom.inventoryservice.repository.InventoryRepository;
import com.andaniom.inventoryservice.service.InventoryService;
import com.andaniom.sharedlibrary.exception.InsufficientStockException;
import com.andaniom.sharedlibrary.exception.NotFoundException;
import com.andaniom.sharedlibrary.response.Response;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;

@Slf4j
@Service
public class InventoryServiceImpl implements InventoryService {

    @Resource
    private InventoryRepository inventoryRepository;

    @Override
    public Response<Object> getProductStock(String productId) {
        Inventory inventory = inventoryRepository.findByProductId(productId);
        if (Objects.isNull(inventory)) {
            throw new NotFoundException(Constant.Message.INVENTORY_NOT_FOUND);
        }
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(inventory)
                .build();
    }

    @Override
    public Response<Object> updateProductStock(String productId, int quantity) {
        Inventory inventory = inventoryRepository.findByProductId(productId);
        if (inventory != null) {
            inventory.setStock(quantity);
            inventoryRepository.save(inventory);
        } else {
            throw new NotFoundException("Product with ID " + productId + " not found in inventory.");
        }
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data("Stock updated successfully")
                .build();
    }

    @Override
    public Response<Object> adjustProductStock(String productId, int quantityChange) {
        Inventory inventory = inventoryRepository.findByProductId(productId);
        if (inventory != null) {
            int newStock = inventory.getStock() + quantityChange;
            inventory.setStock(newStock);
            inventoryRepository.save(inventory);
        } else {
            throw new NotFoundException("Product with ID " + productId + " not found in inventory.");
        }
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data("Adjust Product Stock successfully")
                .build();
    }

    @Override
    public Response<Object> reserveStockForOrder(String productId, int quantity) {
        Inventory inventory = inventoryRepository.findByProductId(productId);
        if (inventory != null) {
            int currentStock = inventory.getStock();
            int currentReservedStock = inventory.getReservedStock();

            // Check if there is enough stock available to reserve
            if (currentStock >= quantity) {
                inventory.setStock(currentStock - quantity);
                inventory.setReservedStock(currentReservedStock + quantity);
                inventoryRepository.save(inventory);
            } else {
                throw new InsufficientStockException("Insufficient stock available for product with ID " + productId);
            }
        } else {
            throw new NotFoundException("Product with ID " + productId + " not found in inventory.");
        }
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data("Reserve Stock For Order successfully")
                .build();
    }

    @Override
    public Response<Object> releaseReservedStock(String productId, int quantity) {
        Inventory inventory = inventoryRepository.findByProductId(productId);
        if (inventory != null) {
            int currentReservedStock = inventory.getReservedStock();

            // Check if there is enough stock available to reserve
            if (currentReservedStock >= quantity) {
                inventory.setReservedStock(currentReservedStock - quantity);

                inventory.setStock(inventory.getStock() + quantity);

                inventoryRepository.save(inventory);
            } else {
                throw new InsufficientStockException("Insufficient reserved stock for product with ID " + productId);
            }
        } else {
            throw new NotFoundException("Product with ID " + productId + " not found in inventory.");
        }
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data("Release Reserved Stock successfully")
                .build();
    }

    @Override
    public Response<Object> confirmOrderStockReservation(String productId, int quantity) {
        Inventory inventory = inventoryRepository.findByProductId(productId);
        if (inventory != null) {
            int currentReservedStock = inventory.getReservedStock();

            // Check if there is enough reserved stock to release
            if (currentReservedStock >= quantity) {
                inventory.setReservedStock(currentReservedStock - quantity);

                inventoryRepository.save(inventory);
            } else {
                throw new InsufficientStockException("Insufficient reserved stock for product with ID " + productId);
            }
        } else {
            throw new NotFoundException("Product with ID " + productId + " not found in inventory.");
        }
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data("Confirm Order Stock Reservation successfully")
                .build();
    }

    public Response<Object> initializeStockForNewProduct(String productId, int stock) {
        log.info("initializeStockForNewProduct");
        Inventory inventory = inventoryRepository.findByProductId(productId);
        if (inventory != null) {
            return Response.builder()
                    .responseCode(Constant.Response.SUCCESS_CODE)
                    .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                    .data(inventory)
                    .build();
        }

        log.info("create inventory");
        inventory = inventoryRepository.save(Inventory.builder()
                .productId(productId)
                .stock(stock)
                .reservedStock(0)
                .createdDate(new Date())
                .modifiedDate(new Date())
                .isDeleted(false).build());
        return Response.builder()
                .responseCode(Constant.Response.SUCCESS_CODE)
                .responseMessage(Constant.Response.SUCCESS_MESSAGE)
                .data(inventory)
                .build();
    }
}
