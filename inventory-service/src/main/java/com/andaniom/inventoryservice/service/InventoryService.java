package com.andaniom.inventoryservice.service;

import com.andaniom.sharedlibrary.response.Response;

public interface InventoryService {

    Response<Object> getProductStock(String productId);

    Response<Object> updateProductStock(String productId, int quantity);

    Response<Object> adjustProductStock(String productId, int quantityChange);

    Response<Object> reserveStockForOrder(String productId, int quantity);

    Response<Object> releaseReservedStock(String productId, int quantity);

    Response<Object> confirmOrderStockReservation(String productId, int quantity);

    Response<Object> initializeStockForNewProduct(String productId, int stock);
}
