package com.andaniom.inventoryservice.controller;

import com.andaniom.inventoryservice.service.InventoryService;
import com.andaniom.sharedlibrary.annotation.AccessToken;
import com.andaniom.sharedlibrary.annotation.PermissionCheckCustom;
import jakarta.annotation.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/inventory")
public class InventoryController {

    @Resource
    private InventoryService inventoryService;


    @GetMapping("/stock")
    public ResponseEntity<?> getProductStock(@RequestParam String productId) {
        return ResponseEntity.ok(inventoryService.getProductStock(productId));
    }

    @AccessToken
    @PermissionCheckCustom
    @PostMapping("/updateStock")
    public ResponseEntity<?> updateProductStock(
            @RequestParam String productId,
            @RequestParam int quantity) {
        return ResponseEntity.ok(inventoryService.updateProductStock(productId, quantity));
    }

    @AccessToken
    @PermissionCheckCustom
    @PostMapping("/adjustStock")
    public ResponseEntity<?> adjustProductStock(
            @RequestParam String productId,
            @RequestParam int quantityChange) {

        return ResponseEntity.ok(inventoryService.adjustProductStock(productId, quantityChange));
    }

//    @AccessToken
//    @PostMapping("/reserveStockRequestParam")
//    public ResponseEntity<?> reserveStockForOrder(
//            @RequestParam String productId,
//            @RequestParam int quantity) {
//        return ResponseEntity.ok(inventoryService.reserveStockForOrder(productId, quantity));
//    }
//
//    @AccessToken
//    @PostMapping("/releaseReservedStockRequestParam")
//    public ResponseEntity<?> releaseReservedStock(
//            @RequestParam String productId,
//            @RequestParam int quantity) {
//        return ResponseEntity.ok(inventoryService.releaseReservedStock(productId, quantity));
//    }
//
//    @AccessToken
//    @PostMapping("/confirmOrderStockReservation")
//    public ResponseEntity<?> confirmOrderStockReservation(
//            @RequestParam String productId,
//            @RequestParam int quantity) {
//        return ResponseEntity.ok(inventoryService.confirmOrderStockReservation(productId, quantity));
//    }
}
