package com.andaniom.inventoryservice.message.consumer;

import com.andaniom.inventoryservice.service.InventoryService;
import com.andaniom.sharedlibrary.dto.ProductDTO;
import com.google.gson.Gson;
import jakarta.annotation.Resource;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class ProductConsumer {

    @Resource
    private InventoryService inventoryService;

    @KafkaListener(topics = "${kafka.topic.product.create-product}", groupId = "${kafka.consumer.group-id}")
    public void createProductConsumer(ConsumerRecord<String, String> record) {
        System.out.println("Received Message: " + record.value());
        ProductDTO productDTO = new Gson().fromJson(record.value(), ProductDTO.class);
        inventoryService.initializeStockForNewProduct(productDTO.getId(), productDTO.getQuantity());
    }
}
