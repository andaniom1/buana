package com.andaniom.inventoryservice.message.consumer;

import com.andaniom.inventoryservice.service.InventoryService;
import com.andaniom.sharedlibrary.dto.OrderDTO;
import com.google.gson.Gson;
import jakarta.annotation.Resource;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class OrderConsumer {

    @Resource
    private InventoryService inventoryService;

    @KafkaListener(topics = "${kafka.topic.order.create-order}", groupId = "${kafka.consumer.group-id}")
    public void createOrderConsumer(ConsumerRecord<String, String> record) {
        System.out.println("Received Message: " + record.value());
        OrderDTO orderDTO = new Gson().fromJson(record.value(), OrderDTO.class);
        inventoryService.reserveStockForOrder(orderDTO.getProductId(), orderDTO.getQuantity());
    }

    @KafkaListener(topics = "${kafka.topic.order.success-order}", groupId = "${kafka.consumer.group-id}")
    public void successOrderConsumer(ConsumerRecord<String, String> record) {
        System.out.println("Received Message: " + record.value());
        OrderDTO orderDTO = new Gson().fromJson(record.value(), OrderDTO.class);
        inventoryService.confirmOrderStockReservation(orderDTO.getProductId(), orderDTO.getQuantity());
    }

    @KafkaListener(topics = "${kafka.topic.order.cancel-order}", groupId = "${kafka.consumer.group-id}")
    public void cancelOrderConsumer(ConsumerRecord<String, String> record) {
        System.out.println("Received Message: " + record.value());
        OrderDTO orderDTO = new Gson().fromJson(record.value(), OrderDTO.class);
        inventoryService.releaseReservedStock(orderDTO.getProductId(), orderDTO.getQuantity());
    }
}
