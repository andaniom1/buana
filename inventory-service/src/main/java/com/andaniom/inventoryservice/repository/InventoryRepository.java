package com.andaniom.inventoryservice.repository;

import com.andaniom.inventoryservice.entity.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InventoryRepository extends JpaRepository<Inventory, String> {

    Inventory findByProductId(String productId);
}