package com.andaniom.inventoryservice.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "tbl_inventory")
public class Inventory {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    private String id;

    @Column(nullable = false)
    private String productId;

    @Column(nullable = false)
    private int stock;

    @Column(nullable = false)
    private int reservedStock;

    private String status;

    private Date createdDate;

    private Date modifiedDate;

    private Boolean isDeleted;
}