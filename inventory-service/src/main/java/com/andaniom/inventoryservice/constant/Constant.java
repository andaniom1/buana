package com.andaniom.inventoryservice.constant;

public interface Constant extends com.andaniom.sharedlibrary.constant.Constant {

    class Message {
        public static final String EXIST_DATA_MESSAGE = "data already exist";
        public static final String INVENTORY_NOT_FOUND = "inventory not found";
        public static final String FORBIDDEN_REQUEST_MESSAGE = "Different {value} with exist data is forbidden";
    }


}
